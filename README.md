# README #

This is a Led test packet which contain server application and client (bash script).

# compile
1) cd led_control
2) make
3) testing...

# Using
./led_control &

While starting, server creates two files:
/tmp/fifo0001.1
/tmp/fifo0001.2

Create log file in current directory: led.log

Some commands for bash script:

./led_client.sh set_state of
./led_client.sh set_state off

./led_client.sh set_state on
./led_client.sh get_state

./led_client.sh set_rate 2
./led_client.sh get_rate

./led_client.sh set_color 2 3 4
./led_client.sh get_color