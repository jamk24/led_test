// Led middleware
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>

#include "wlog.h"
#include "routine.h"
#include "cmd_handler_fifo.h"

int main_work = 1;
pthread_t   ptcmd_id;



int sterm(int sig);


int main(int argc, char *argv[])
{
	char        sbuf[300];
	u_int32_t   buf;
	int         pid;
	int         buf1;

	write_log("Led server started...");

	//make_pid(PID_FILE, getpid());
	// ������������� ����������� ��������
	signal(SIGTERM, (void *)sterm);
	signal(SIGKILL, (void *)sterm);
	signal(SIGINT,  (void *)sterm);

	// ������� ���������� �� ���������
	//if ((buf = pthread_mutex_init(&traf_lock, NULL)) < 0)
	//{
		//write_log("error create traf_lock...");
		//exit(1);
	//}
	init_handlers();
 
	// create thread for command handler
	buf = pthread_create(&ptcmd_id, NULL, (void *)&cmd_server, NULL);

	main_work = 1;
	write_log("Led server start...");
	// ������� ����

	while (main_work)
	{

		if ((!s_work) && (main_work))
		{
			write_log("abnormal termination cmd_handler_fifo...");
			main_work = 0;
		}
		
		
		sleep(1);
	}

	write_log("Led server stopping...");

	// ������� ����������
	//pthread_mutex_destroy(&evnt_lock);

	exit(0);
}



int sterm(int sig)
{
	// Terminate main process
	s_work = 0;
	main_work = 0;
	
	write_log("software termination...");
	//printf("software termination...\n");
	return(0);
}

