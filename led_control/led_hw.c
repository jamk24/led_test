// led hardware layer

#include "led_hw.h"


//static struct led_hw	*ld_hw;


int led_hw_open_dev(struct led_hw *l_hw, char *led_dev)
{
	// init :)
	l_hw->state = 0;
	l_hw->red = 0;
	l_hw->green = 0;
	l_hw->blue = 0;
	l_hw->blink_rate = 0;

	return(LED_OK);
}




int led_hw_set_state(struct led_hw *l_hw, unsigned char state)
{

	if (state >= 2)
	{
		return(LED_ERROR);
	}
	
	l_hw->state = state;

	return(LED_OK);

}


int led_hw_get_state(struct led_hw *l_hw, unsigned char *state)
{

	*state = l_hw->state;

	return(LED_OK);
}

int led_hw_set_color(struct led_hw *l_hw, unsigned char red, unsigned char green, unsigned char blue)
{

	l_hw->red = red;
	l_hw->green = green;
	l_hw->blue = blue;

	return(LED_OK);

}


int led_hw_get_color(struct led_hw *l_hw, unsigned char *red, unsigned char *green, unsigned char *blue)
{

	*red = l_hw->red;
	*green = l_hw->green;
	*blue = l_hw->blue;

	return(LED_OK);
}


int led_hw_set_rate(struct led_hw *l_hw, unsigned char rate)
{

	if (rate > 5)
	{
		return(LED_ERROR);
	}
	l_hw->blink_rate = rate;

	return(LED_OK);

}


int led_hw_get_rate(struct led_hw *l_hw, unsigned char *rate)
{

	*rate = l_hw->blink_rate;

	return(LED_OK);
}

