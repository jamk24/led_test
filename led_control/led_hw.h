#ifndef _LEDHW_H
#define _LEDHW_H

#include <stdio.h>
#include <string.h>

#define    HW_LED0		"/dev/led0"

#define    LED_OK		1
#define    LED_ERROR		-1


struct led_hw
{
    unsigned char	state;
    unsigned char	red;
    unsigned char	green;
    unsigned char	blue;
    unsigned char	blink_rate;
};



int led_hw_open_dev(struct led_hw *l_hw, char *led_dev);
int led_hw_set_state(struct led_hw *l_hw, unsigned char state);
int led_hw_get_state(struct led_hw *l_hw, unsigned char *state);
int led_hw_set_color(struct led_hw *l_hw, unsigned char red, unsigned char green, unsigned char blue);
int led_hw_get_color(struct led_hw *l_hw, unsigned char *red, unsigned char *green, unsigned char *blue);
int led_hw_set_rate(struct led_hw *l_hw, unsigned char rate);
int led_hw_get_rate(struct led_hw *l_hw, unsigned char *rate);



#endif
