#ifndef _CMD_HANDLER_H
#define _CMD_HANDLER_H

#include <stdio.h>
#include <stdlib.h>
#include <sys/file.h>
#include <time.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>


#define  FIFO_FILE		"/tmp/fifo0001.1"
#define  FIFO_CHMOD		"/bin/chmod 0666 " || FIFO_FILE
#define  FIFO_FILE_WR		"/tmp/fifo0001.2"
#define  FIFO_CHMOD_WR		"/bin/chmod 0666 " || FIFO_FILE_WR
#define  FIFO_READ_TIMEOUT	100000
#define  MAX_CMD_BUF		255
#define  MAX_CMD_LEN		128
#define  RET_CARRET		0x0A
#define  CMD_OK			0
#define  CMD_ERROR		-1
#define  RESPONSE_OK		"OK"
#define  RESPONSE_ERR		"FAILED"



#define  com_len		6


static char     commands[com_len][20] = {"set-led-state",\
					 "get-led-state",\
					 "set-led-color",\
					 "get-led-color",\
					 "set-led-rate",\
					 "get-led-rate"};
static int s_work = 1;

int      (*handlers[com_len])();

static struct led_hw *ld_hw;

void cmd_server();
void init_handlers();
int pos_command(char *st);

int set_led_state(char *orig_buf, char *cmd_arg, char *ret_buf);
int get_led_state(char *orig_buf, char *cmd_arg, char *ret_buf);
int set_led_color(char *orig_buf, char *cmd_arg, char *ret_buf);
int get_led_color(char *orig_buf, char *cmd_arg, char *ret_buf);
int set_led_rate(char *orig_buf, char *cmd_arg, char *ret_buf);
int get_led_rate(char *orig_buf, char *cmd_arg, char *ret_buf);

#endif
