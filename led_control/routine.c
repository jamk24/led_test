#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "routine.h"


void getstring(char *stt, char *splt, int n, char *outstr)
{
    int cnt;
    char *word, *brkt, *sttt;
    char stt1[360];
 
 
    word = NULL;
    brkt = NULL;
    sttt = stt1;
    
    strcpy(sttt, stt);

    cnt = 1;
 
    for (word = strtok_r(sttt, splt, &brkt);word;word = strtok_r(NULL, splt, &brkt))
    {
	if (cnt == n)
	{
	    strcpy(outstr, word);
	}

	cnt++;
    }
  
    return;
}

void getvalue(char *stt, char *fst, char *outstr)
{
    int   i;
    char  sbuf[360];
    char  fst1[360];
 
    outstr[0] = 0;

    i = 1;
    while (i < 30)
    {
	sbuf[0] = 0;

	getstring(stt, ";", i, sbuf);
	//printf("getvalue: sbuf: %s, cnt: %d\n", sbuf, i);
	if (!sbuf[0])
	{
    	    break;
	}
	else
	{
	    fst1[0] = 0;
	    
	    getstring(sbuf, "=", 1, fst1);
	    if (!strcmp(fst1, fst))
	    {
		// zero value
		fst1[0] = 0;
		getstring(sbuf, "=", 2, fst1);
		strcpy(outstr, fst1);
		return;
	    }
	}
	i++;
    }

    return;
}
