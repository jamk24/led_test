#include "wlog.h"

void write_log(char *st)
{
	FILE       *ff;
	int        ffd;
	time_t     tmp;
	char       date_s[1024];

	memset(&date_s, 0, sizeof(date_s));

	// check len
	if (strlen(st) >= 1024)
	{
		st[strlen(st) - 1] = 0;
	}

	ff = fopen(LOG_FILE, "a");
	if (!ff) return;
	ffd = fileno(ff);
	
	time(&tmp);
	strftime(date_s, sizeof(date_s), "%d.%m.%Y %H:%M:%S ", localtime(&tmp));
	strcat(date_s, st);
	flock(ffd, LOCK_EX);
	fprintf(ff, "%s\n", date_s);
	flock(ffd, LOCK_UN);
	fclose(ff);

	// syslog
	//WriteLog(SYSL_INFO, date_s, strlen(date_s), syslog_srv, 514);

}

