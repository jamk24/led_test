// command handler (pthread)
#include <errno.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "cmd_handler_fifo.h"
#include "led_hw.h"
#include "wlog.h"


//int s_work = 1;

// receive commands from fifo
void cmd_server()
{
	int     fd_fifo;
	int     fd_fifo_wr;
	char    sbuf[MAX_CMD_BUF];
	char    sbuf1[MAX_CMD_BUF];
	char    cmd[MAX_CMD_BUF];
	char    cmd_arg[MAX_CMD_BUF];
	char    ret_buf[MAX_CMD_BUF];
	
	int     len,i,l,full,num_command;
	fd_set  fd_input;
	int     maxFD;
	int     pendingFD;
	struct timeval timeout;

	// if fifo is exist - remove it
	unlink(FIFO_FILE);
	unlink(FIFO_FILE_WR);

	// make new fifo read
	if ((mkfifo(FIFO_FILE, O_RDWR)) == -1)
	{
		write_log("error while create read fifo channel (cmd_handler_fifo)...");
		s_work = 0;
		pthread_exit(NULL);
	}

	// make new fifo write
	if ((mkfifo(FIFO_FILE_WR, O_RDWR)) == -1)
	{
		write_log("error while create write fifo channel (cmd_handler_fifo)...");
		s_work = 0;
		pthread_exit(NULL);
	}

	// chmod
	//system(FIFO_CHMOD);
	//system(FIFO_CHMOD_WR);

	// open read channel
	if ((fd_fifo = open(FIFO_FILE, O_RDWR)) == - 1)
	{
		write_log("error open read fifo in (cmd_handler_fifo)...");
		s_work = 0;
		pthread_exit(NULL);
	}

	// open write channel
	if ((fd_fifo_wr = open(FIFO_FILE_WR, O_RDWR)) == - 1)
	{
		write_log("error open write fifo in (cmd_handler_fifo)...");
		s_work = 0;
		pthread_exit(NULL);
	}
	
	// open led hw
	ld_hw = malloc(sizeof(struct led_hw));
	if (led_hw_open_dev(ld_hw, HW_LED0) != LED_OK)
	{
		write_log("error open led hardware (cmd_handler_fifo)...");
		s_work = 0;
		pthread_exit(NULL);
	}
	
	//buf1[0] = 0;
	//sbuf[0] = 0;
	//l = 0;
	//full = 0;
	//memset(&buf, 0, 255);
	write_log("cmd_handler_fifo started...");

	while (s_work)
	{
		FD_ZERO(&fd_input);
		FD_SET(fd_fifo, &fd_input);
		maxFD = fd_fifo + 1;
		
		
		bzero(&timeout, sizeof(timeout));
		timeout.tv_sec  = 0;
		timeout.tv_usec = FIFO_READ_TIMEOUT;
		
		pendingFD = select(maxFD, &fd_input, NULL, NULL, &timeout);
		
		if (pendingFD > 0)
		{
			bzero((void *)&sbuf, sizeof(sbuf));
		
			if ((len = read(fd_fifo, &sbuf, MAX_CMD_BUF)) < 0)
			{
				write_log("error while read from fifo in (cmd_handler_fifo)...");
				s_work = 0;
				break;
			}
			
			bzero((void *)&sbuf1, sizeof(sbuf1));
			
			if ((len < MAX_CMD_LEN) && (sbuf[len - 1] == RET_CARRET))
			{
				
				sprintf(sbuf1, "recv cmd, len = %d, str: %s", len, sbuf);
				write_log(sbuf1);
				
				int ccount;
				
				// clear
				bzero((void *)&cmd, sizeof(cmd));
				bzero((void *)&cmd_arg, sizeof(cmd_arg));
				
				// get cmd and arguments
				ccount = sscanf(sbuf, "%s %s\n", cmd, cmd_arg);
				
				num_command = -1;
				// get offset of cmd
				if (ccount >= 1)
				{
					num_command = pos_command(cmd);
				}
				
				
				if (num_command >= 0)
				{
					sbuf1[0] = 0;
					sprintf(sbuf1, "cmd: %s, cmd_arg: %s, handler offset: %d", cmd, cmd_arg, num_command);
					write_log(sbuf1);
				
					// call handler
					int res = handlers[num_command]((char *)&sbuf, (char *)&cmd_arg, (char *)&ret_buf);
					
					if (res == CMD_OK)
					{
						int w_status = write(fd_fifo_wr, ret_buf, strlen(ret_buf));
						
						if (w_status < 0)
						{
							write_log("error write to fifo...");
						}
					
					}
					else
					{
						write_log("error cmd handle...");
					}
				}
				else
				{
					sbuf1[0] = 0;
					sprintf(sbuf1, "cmd: %s, not found", cmd);
					write_log(sbuf1);
				}
			}
			else
			{
			
				write_log("wrong format of cmd...");
			}
		}
		
	}



	close(fd_fifo);
	close(fd_fifo_wr);
	free(ld_hw);
	write_log("cmd_handler_fifo stopped...");
	pthread_exit(NULL);
}


void init_handlers()
{
	handlers[0] = set_led_state;
	handlers[1] = get_led_state;
	handlers[2] = set_led_color;
	handlers[3] = get_led_color;
	handlers[4] = set_led_rate;
	handlers[5] = get_led_rate;
}

// ���������� ������ ������� � ������� ������
int pos_command(char *st)
{
	int   i;
	int   res;

	for (i = 0;i < com_len;i++)
	{
		res = strcmp(st, commands[i]);
		if (!res)
		{
			return(i);
		}
	}

	return(-1);
}


// set led state
int set_led_state(char *orig_buf, char *cmd_arg, char *ret_buf)
{
	char    sbuf[MAX_CMD_BUF];
	int     cmd_state, ccount;
	unsigned char state;
	
	//write_log(cmd_arg);
	
	//ccount = sscanf(cmd_arg, "%d", cmd_state);
	//sprintf(sbuf, "ccount: %d, arg_state: %d", ccount, cmd_state);
	//write_log(sbuf);
	
	cmd_state = atoi(cmd_arg);
	
	
	if (ld_hw != NULL)
	{
		state = (unsigned char)cmd_state;
		
		if (led_hw_set_state(ld_hw, state) == LED_OK)
		{
		
		
			sprintf(ret_buf, "%s\n", RESPONSE_OK);
		}
		else
		{
			sprintf(ret_buf, "%s\n", RESPONSE_ERR);
		}
		

	}
	else
	{
		sprintf(ret_buf, "%s\n", RESPONSE_ERR);
	}

	//sprintf(ret_buf, "OK\n");
	return(CMD_OK);
}


// get led state
int get_led_state(char *orig_buf, char *cmd_arg, char *ret_buf)
{
	char    sbuf[MAX_CMD_BUF];
	unsigned char state;

	if (ld_hw != NULL)
	{
		if (led_hw_get_state(ld_hw, (unsigned char *)&state) == LED_OK)
		{
		
			sprintf(ret_buf, "%s %d\n", RESPONSE_OK, state);
		}
		else
		{
			sprintf(ret_buf, "%s\n", RESPONSE_ERR);
		}
		

	}
	else
	{
		sprintf(ret_buf, "%s\n", RESPONSE_ERR);
	}
	
	return(CMD_OK);

}

// set led color
int set_led_color(char *orig_buf, char *cmd_arg, char *ret_buf)
{
	char    sbuf[MAX_CMD_BUF];
	int     cmd_state, ccount;
	int     lred,lgreen,lblue;
	unsigned char red, green, blue;

	write_log(cmd_arg);
	
	ccount = sscanf(cmd_arg, "%d,%d,%d", (int *)&lred, (int *)&lgreen, (int *)&lblue);
	if (ccount == 3)
	{
		sprintf(sbuf, "ccount: %d, args: %d %d %d", ccount, lred, lgreen, lblue);
		write_log(sbuf);
		
		if ((lred >= 0 && lred <= 255) && (lgreen >= 0 && lgreen <= 255) && (lblue >= 0 && lblue <= 255))
		{
			red = (unsigned char)lred;
			green = (unsigned char)lgreen;
			blue = (unsigned char)lblue;
		}
		else
		{
			sprintf(ret_buf, "%s\n", RESPONSE_ERR);
			return(CMD_OK);
		}
		
		if (ld_hw != NULL)
		{
			if (led_hw_set_color(ld_hw, red, green, blue) == LED_OK)
			{
		
				sprintf(ret_buf, "%s\n", RESPONSE_OK);
			}
			else
			{
				sprintf(ret_buf, "%s\n", RESPONSE_ERR);
			}
		
		}
		else
		{
			sprintf(ret_buf, "%s\n", RESPONSE_ERR);
		}
	}
	else
	{
		sprintf(ret_buf, "%s\n", RESPONSE_ERR);
	}
	
	return(CMD_OK);
}


// get led color
int get_led_color(char *orig_buf, char *cmd_arg, char *ret_buf)
{
	char    sbuf[MAX_CMD_BUF];
	unsigned char red, green, blue;

	if (ld_hw != NULL)
	{
		if (led_hw_get_color(ld_hw, (unsigned char *)&red, (unsigned char *)&green, (unsigned char *)&blue) == LED_OK)
		{
	
			sprintf(ret_buf, "%s %d|%d|%d\n", RESPONSE_OK, red, green, blue);
		}
		else
		{
			sprintf(ret_buf, "%s\n", RESPONSE_ERR);
		}
		
	}
	else
	{
		sprintf(ret_buf, "%s\n", RESPONSE_ERR);
	}

	return(CMD_OK);


}

// set led rate
int set_led_rate(char *orig_buf, char *cmd_arg, char *ret_buf)
{
	char    sbuf[MAX_CMD_BUF];
	int     cmd_rate, ccount;
	unsigned char rate;
	
	cmd_rate = atoi(cmd_arg);
	
	
	if (ld_hw != NULL)
	{
		rate = (unsigned char)cmd_rate;
		
		if (led_hw_set_rate(ld_hw, rate) == LED_OK)
		{
		
			sprintf(ret_buf, "%s\n", RESPONSE_OK);
		}
		else
		{
			sprintf(ret_buf, "%s\n", RESPONSE_ERR);
		}
		
	}
	else
	{
		sprintf(ret_buf, "%s\n", RESPONSE_ERR);
	}

	return(CMD_OK);


}


// get led color
int get_led_rate(char *orig_buf, char *cmd_arg, char *ret_buf)
{
	char    sbuf[MAX_CMD_BUF];
	unsigned char rate;

	if (ld_hw != NULL)
	{
		if (led_hw_get_rate(ld_hw, (unsigned char *)&rate) == LED_OK)
		{
		
			sprintf(ret_buf, "%s %d\n", RESPONSE_OK, rate);
		}
		else
		{
			sprintf(ret_buf, "%s\n", RESPONSE_ERR);
		}
		

	}
	else
	{
		sprintf(ret_buf, "%s\n", RESPONSE_ERR);
	}

	return(CMD_OK);


}

