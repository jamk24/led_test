#!/bin/bash

pipe=/tmp/fifo0001.1
pipe1=/tmp/fifo0001.2

if [ ! -p $pipe ]; then
    echo "server not running..."
    exit 1
fi


echo 'send cmd....'

if [ "$1" == "set_state" ] ; then
    
    if [ "x$2" != "x" ]; then
    
	if [ "$2" == "on" ]; then
	    echo 'set-led-state 1' > $pipe
	fi
	
	if [ "$2" == "off" ]; then
	    echo 'set-led-state 0' > $pipe
	#else
	#    echo 'Wrong state, use on/off'
	#    exit
	fi
	
    else
    
	echo 'Usage: ./led_client.sh set_state on/off'
	exit
    fi

fi

if [ "$1" == "set_rate" ] ; then
    
    if [ "x$2" != "x" ]; then
    
	echo 'set-led-rate '$2 > $pipe
	
    else
    
	echo 'Usage: ./led_client.sh set_rate <1..5>'
	exit
    fi

fi



if [ "$1" == "get_state" ] ; then
    echo 'get-led-state' > $pipe
    
fi

if [ "$1" == "get_rate" ] ; then
    echo 'get-led-rate' > $pipe
    
fi


if [ "$1" == "set_color" ] ; then
    if [ "x$2" != "x" ]; then
	if [ "x$3" != "x" ]; then
	    if [ "x$4" != "x" ]; then
		echo 'set-led-color '$2','$3','$4 > $pipe
	    fi
	fi
    fi
fi


if [ "$1" == "get_color" ] ; then
    echo 'get-led-color' > $pipe
    
fi


while true
do
    if read line <$pipe1; then
    
	ret=`echo $line | awk '{ print $1 }'`
	code=`echo $line | awk '{ print $2 }'`
	
	if [ "$ret" == "OK" ]; then
	    if [ "$1" == "get_state" ]; then
		if [ "$code" == "1" ]; then
		    echo 'LED on'
		    break
		fi
		if [ "$code" == "0" ]; then
		    echo 'LED off'
		    break
		fi
	    
	    fi

	    if [ "$1" == "get_rate" ]; then
		    echo 'Rate: '$code
		    break
	    fi
	    
	    if [ "$1" == "get_color" ]; then
		    #echo 'color: '$code
		    color=(${code//|/ });
		    #echo "luke;yoda;leila" | tr "|" " "
		    echo 'Color red:'${color[0]}' green:'${color[1]}' blue:'${color[2]}
		    break
	    fi
	    
	    
	    
	fi
        
        echo $line
        break
    fi
done
